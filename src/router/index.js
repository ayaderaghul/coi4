import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/Home'
import Dangnhap from '@/components/Dangnhap'
import Profile from '@/components/Profile'
import Dothi from '@/components/Dothi'
import Thongtin from '@/components/Thongtin'
import Napthem from '@/components/Napthem'
import Tonghop from '@/components/Tonghop'
import Taikhoan from '@/components/Taikhoan'
import Dautu from '@/components/Dautu'
import Lichsu from '@/components/Lichsu'
import Tienich from '@/components/Tienich'
import Ruttien from '@/components/Ruttien'
import Canhan from '@/components/Canhan'
import Ungtien from '@/components/Ungtien'
import Huongdan from '@/components/Huongdan'
import Doimatkhau from '@/components/Doimatkhau'
import Faq from '@/components/Faq'
import Ykien from '@/components/Ykien'
import Lienhe from '@/components/Lienhe'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
	{
	    path: '/',
	    name: 'Home',
	    component: Home
	},
	{
	    path: '/dangnhap',
	    name: 'Dangnhap',
	    component: Dangnhap
	},
	{
	    path: '/profile',
	    name: 'Profile',
	    component: Profile,
	    children: [
		{
		    path: 'napthem',
		    name: 'Napthem',
		    component: Napthem
		},
		{
		    path: 'ruttien',
		    name: 'Ruttien',
		    component: Ruttien
		},
		{
		    path: 'tonghop',
		    name: 'Tonghop',
		    component: Tonghop,
		    
		},
		{
		    path: 'taikhoan',
		    name: 'Taikhoan',
		    component: Taikhoan
		},
		{
		    path: 'dautu',
		    name: 'Dautu',
		    component: Dautu,
		    children: [
			{
			    path: 'dothi',
			    name: 'Dothi',
			    component: Dothi
			},
			{
			    path: 'thongtin',
			    name: 'Thongtin',
			    component: Thongtin
			}
		    ]
		},
		{
		    path: 'lichsu',
		    name: 'Lichsu',
		    component: Lichsu,
		},
		{
		    path: 'tienich',
		    name: 'Tienich',
		    component: Tienich,
		},
		{
		    path: 'canhan',
		    name: 'Canhan',
		    component: Canhan,
		},
		{
		    path: 'ungtien',
		    name: 'Ungtien',
		    component: Ungtien,
		},
		{
		    path: 'huongdan',
		    name: 'Huongdan',
		    component: Huongdan,
		},
		{
		    path: 'doimatkhau',
		    name: 'Doimatkhau',
		    component: Doimatkhau
		},
		{
		    path: 'faq',
		    name: 'Faq',
		    component: Faq
		},
		{
		    path: 'ykien',
		    name: 'Ykien',
		    component: Ykien,
		},
		{
		    path: 'lienhe',
		    name: 'Lienhe',
		    component: Lienhe,
		}
		
		
		 
	    ]
	}
    ]
})
